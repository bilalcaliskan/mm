# Terraform with AWS

- https://hands-on.cloud/terraform-managing-aws-vpc-creating-public-subnet/
- https://hands-on.cloud/terraform-managing-aws-vpc-creating-private-subnets/
- https://hands-on.cloud/terraform-recipe-managing-auto-scaling-groups-and-load-balancers/
- https://github.com/andreivmaksimov/terraform-recipe-managing-auto-scaling-groups-and-load-balancers/blob/master/infrastructure.tf


Check belows for ASG:
  - https://registry.terraform.io/modules/terraform-aws-modules/autoscaling/aws/latest
  - https://github.com/terraform-aws-modules/terraform-aws-autoscaling/tree/master/examples/complete